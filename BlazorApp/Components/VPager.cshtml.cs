﻿#region Imports
using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Components {

    public class CPagerModel : BlazorComponent {
        #region Fields
        [Parameter]
        protected CPagedResultBase Result { get; set; }
        [Parameter]
        protected Action<int> PageChanged { get; set; }
        protected int StartIndex { get; private set; } = 0;
        protected int FinishIndex { get; private set; } = 0; 
        #endregion

        #region OnParametersSet
        protected override void OnParametersSet() {
            CLogger.LogDebug("setou parametros:" + Result.RowCount);
            StartIndex = Math.Max(Result.CurrentPage - 5, 1);
            FinishIndex = Math.Min(Result.CurrentPage + 5, Result.PageCount);
            base.OnParametersSet();
        }
        #endregion

        #region PagerButtonClicked
        protected void PagerButtonClicked(int page) {
            PageChanged?.Invoke(page);
        } 
        #endregion
    }
}
