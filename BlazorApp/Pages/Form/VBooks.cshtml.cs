﻿#region Imports
using BlazorApp.Codes;
using BlazorApp.Components;
using BlazorApp.Entities;
using Microsoft.AspNetCore.Blazor.Browser.Interop;
using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Pages.Form {
    public class VFormModel : CBaseComponent {

        #region Fields
        [Parameter]
        protected string Page { get; set; } = "1";
        protected int DeleteId { get; set; } = 0;
        protected CPagedResult<EBook> Books;
        #endregion

        #region OnParametersSetAsync
        protected override async Task OnParametersSetAsync() {
            Console.WriteLine("Current page: " + Page);
            await LoadBooks(int.Parse(Page));
        }
        #endregion

        #region LoadBooks
        private async Task LoadBooks(int page) {
            //Books = await Http.GetJsonAsync<PagedResult<Book>>("/Books/Index/page/" + page);
            Books = await BooksClient.ListBooks(page);
            CLogger.LogDebug("carregou livros:" + Books.Results.Count + " .Count:" + Books.RowCount+ ".Pagina atual"+Books.CurrentPage+".PageCount:"+Books.PageCount+".PageSize:"+Books.PageSize);
        }
        #endregion

        #region PagerPageChanged
        protected void PagerPageChanged(int page) {
            UriHelper.NavigateTo("/books/" + page);
        }
        #endregion

        #region AddNew
        protected void AddNew() {
            UriHelper.NavigateTo("/booksedit/0");
        }
        #endregion

        #region EditBook
        protected void EditBook(int id) {
            UriHelper.NavigateTo("/booksedit/" + id);
        }
        #endregion

        #region ConfirmDelete
        protected void ConfirmDelete(int id, string title) {
            DeleteId = id;
            RegisteredFunction.Invoke<bool>("confirmDelete", title);
        }
        #endregion

        #region DeleteBook
        protected async Task DeleteBook(int id) {
            //RegisteredFunction.Invoke<bool>("hideDeleteDialog");
            await BooksClient.DeleteBook(id);
            await LoadBooks(int.Parse(Page));
        } 
        #endregion
    }
}
