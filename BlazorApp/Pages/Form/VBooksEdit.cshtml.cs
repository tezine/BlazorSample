﻿#region Imports
using BlazorApp.Codes;
using BlazorApp.Entities;
using Microsoft.AspNetCore.Blazor.Browser.Interop;
using Microsoft.AspNetCore.Blazor.Components;
using Microsoft.AspNetCore.Blazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
#endregion

namespace BlazorApp.Pages.Form {

    public class VBooksEditModel: CBaseComponent {

        #region Fields
        [Parameter]
        protected string Id { get; private set; } = "0";
        [Inject]//o inject do service é realizado assim no arquivo C#
        protected IBooksClient booksClient { get; set; }
        protected string PageTitle { get; set; }
        protected EBook CurrentBook { get; set; }
        #endregion

        #region OnParameterSetAsync
        protected override async Task OnParametersSetAsync() {
            if (Id == null || Id == "0") {
                PageTitle = "Adicionar livro";
                CurrentBook = new EBook();
            } else {
                PageTitle = "Editar livro";
                await LoadBook(int.Parse(Id));
            }
        }
        #endregion

        #region LoadBook
        protected async Task LoadBook(int id) {  
            CurrentBook = await booksClient.GetBook(id);
        }
        #endregion

        #region Save
        protected async Task Save() {
            await booksClient.SaveBook(CurrentBook);
            UriHelper.NavigateTo("/books");
        } 
        #endregion
    }
}
