﻿#region Imports
using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Blazor.Components;
using Microsoft.AspNetCore.Blazor.RenderTree;
#endregion

namespace BlazorApp.Pages {
    public class CButton : BlazorComponent {

        public const string Title = "Hello World - Class Only";
        [Parameter]
        private bool absolute { get; set; } = false;
        [Parameter]
        private string active_class { get; set; } = "btn--active";
        [Parameter]
        private bool append { get; set; } = false;
        [Parameter]
        private bool block { get; set; } = false;
        [Parameter]
        private bool bottom { get; set; } = false;
        [Parameter]
        private string color { get; set; } = null;
        [Parameter]
        private RenderFragment ChildContent { get; set; }

        protected override void BuildRenderTree(RenderTreeBuilder builder) {
            builder.OpenElement(1, "button");
            builder.AddContent(2, Title);
            builder.CloseElement();
        }
    }
}
