﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorRedux; 
#endregion

namespace BlazorApp {
    public static class CReduxReducers {

        #region RootReducer
        public static CReduxFetchDataState RootReducer(CReduxFetchDataState state, IAction action) {
            if (state == null) throw new ArgumentNullException(nameof(state));
            return new CReduxFetchDataState {
                Location = Location.Reducer(state.Location, action),
                Count = CountReducer(state.Count, action),
                Forecasts = ForecastsReducer(state.Forecasts, action)
            };
        }
        #endregion

        #region CountReducer
        private static int CountReducer(int count, IAction action) {
            switch (action) {
                case IncrementByOneAction _:
                    return count + 1;
                case IncrementByValueAction a:
                    return count + a.Value;
                default:
                    return count;
            }
        }
        #endregion

        #region ForecastsReducer
        private static IEnumerable<EWeatherForecast> ForecastsReducer(IEnumerable<EWeatherForecast> forecasts, IAction action) {
            switch (action) {
                case ClearWeatherAction _:
                    return null;
                case ReceiveWeatherAction a:
                    return a.Forecasts;
                default:
                    return forecasts;
            }
        } 
        #endregion
    }
}
