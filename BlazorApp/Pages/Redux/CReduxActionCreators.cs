﻿#region Imports
using BlazorRedux;
using Microsoft.AspNetCore.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks; 
#endregion

namespace BlazorApp {
    public static class CReduxActionCreators {

        #region LoadWeather
        public static async Task LoadWeather(Dispatcher<IAction> dispatch, HttpClient http) {
            dispatch(new ClearWeatherAction());
            var forecasts = await http.GetJsonAsync<EWeatherForecast[]>("/sample-data/weather.json");
            dispatch(new ReceiveWeatherAction {Forecasts = forecasts});
        } 
        #endregion
    }
}
