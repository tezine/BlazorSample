﻿#region Imports
using BlazorRedux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp {
    //repare que esta classe extende ReduxComponent ao invés de BlazorComponent
    public class FetchDataReduxComponent : ReduxComponent<CReduxFetchDataState, IAction> {
    }
}
