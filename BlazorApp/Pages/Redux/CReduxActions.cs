﻿#region Imports
using BlazorRedux;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
#endregion

namespace BlazorApp {
    #region IncrementByOneAction
    public class IncrementByOneAction : IAction {
    }
    #endregion

    #region IncrementByValueAction
    public class IncrementByValueAction : IAction {
        public IncrementByValueAction(int value) {Value = value;}
        public int Value { get; set; }
    }
    #endregion

    #region ClearWeatherAction
    public class ClearWeatherAction : IAction {
    }
    #endregion

    #region ReceiveWeatherAction
    public class ReceiveWeatherAction : IAction {
        public IEnumerable<EWeatherForecast> Forecasts { get; set; }
    } 
    #endregion
}
