﻿#region Imports 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Entities {
    public class EBook {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
    }
}
