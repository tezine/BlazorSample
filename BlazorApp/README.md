# Este App
* Segue o fluxo de execu��o do app: 
1) � executado o m�todo Main do arquivo Program.cs
2) No final da fun��o Main � executado o App.cshtml: `new BrowserRenderer(serviceProvider).AddComponent<App>("app");`
3) O App.cshtml contem a linha `<Router...`. Assim, � carregada a p�gina com a indica��o `@page "/"`. Neste caso, o arquivo Index.cshtml
4) Como o Index.cshtml est� na pasta Pages e a pasta Pages cont�m o arquivo _ViewImports.cshtml contendo a linha `@layout VMainLayout `, o Index.cshtml � renderizado utilizando o VLayout. 

# BLAZOR
* Blazor executa o Runtime do dotnet no browser em webassembly.
* Cria-se arquivos .cshtml utilizando apenas html, css e c#. Com exce��o quando utilizamos outras libs, n�o se utiliza javascript.
* O runtime tem 200 kilobytes. 
* Funciona inclusive em browsers que n�o suportam webassembly. Neste caso, o Blazor faz o download do polyfill automaticamente quando webassembly n�o � suportaddo. 
* Suporta Credential Management. Credential management � uma funcionalidade nova que os browser est�o come�ando a suportar. 
* A depura��o no console � feita atrav�s de `Console.WriteLine("texto");`
* Pode-se executar apenas uma linha em C# usando @seuCodigoC# ou executar v�rias linhas colocando @{seus c�digos C#; }
* Por enquanto, o blazor est� em preview. Para testar, criar um app blazor standalone e executar via "Start without debugging". Debugging ainda n�o � suportado. 
* Blazor suporta libs com .netstandard >2.0, mas v�rias n�o funcionam muito bem ainda. Por exemplo, o Newtonsoft.Json ainda n�o funciona . 
* O Blazor utiliza o mono. O mono usa o IL Linker que faz an�lise est�tica para verificar quais partes do assembly s�o necess�rias no app final e faz um strip de todo o resto que n�o � utilizado. Isso � semelhante ao tre shaking do javascript. 

# INSTALA��O DO TEMPLATE DE PROJETO BLAZOR
* Para instalar o template mais atual do blazor, execute o seguinte comando: <BR>
`dotnet new -i Microsoft.AspNetCore.Blazor.Templates`
* Para criar uma solu��o com 3 projetos (server, client e shared): `dotnet new blazorhosted -o MeuProjeto`
* Para criar um projeto Blazor standalone: `dotnet new blazor -o MeuProjeto`
* Para criar uma lib blazor: `dotnet new blazorlib -o MeuProjeto`

# ESTRUTURA DE DIRET�RIOS
* Um projeto blazor � estruturado da seguinte maneira: 
```
App/
    wwwroot/
        css/
        index.html <- primeira p�gina a ser renderizada. Onde ficam os css
    Pages/
        Index.cshtml <- primeiro cshtml a ser renderizado. Adicione rota componentes aqui
        _ViewImports.cshtml
    Services/
        MeuServico.cs
    Shared/
        MainLayout.cshtml
    _ViewImports.cshtml
    App.cshtml
    global.json
    Program.cs
```
* O arquivo MainLayout.cshtml define o layout do SPA. Segue um exemplo de MainLayout.cshtml abaixo:
```
@inherits BlazorLayoutComponent

<div class="sidebar">
    <NavMenu />
</div>

<div class="main">
    <div class="top-row px-4">
        <a href="http://blazor.net" target="_blank" class="ml-md-auto">About</a>
    </div>

    <div class="content px-4">
        @Body
    </div>
</div>
```

# COMPONENTES
* Cada arquivo .cshtml define um componente Blazor.
Exemplo de arquivo .cshtml: 
```
@page "/counter"
<h1>Counter</h1>
<p>Current count: @currentCount</p>
<button @onclick(IncrementCount)>Click me</button>

@functions {
    [Parameter]
    private int currentCount { get; set; } =0;

    void IncrementCount(){
        currentCount++;
    }
}
```  
* Podemos utilizar o componente acima em outro componente. Para isso, basta adicionar a linha <Counter currentCount="10"></Counter>. Repare que estamos passando um par�metro `currentCount="10"` para o componente. No exemplo acima, a primeira linha @page "/counter" indica que h� uma rota /counter para abrir esse componente. Caso esse componente s� seja usado dentro de outros componentes, n�o precisamos adicionar essa linha. 
* Um componente Blazor pode ser utilizado em outro projeto.
* Um child component pode renderizar o conte�do fornecido pelo parent da seguinte maneira: 
```
//parent component:
@page "/ParentComponent"
<h1>Parent-child example</h1>
<ChildComponent Title="Panel Title from Parent">
    Child content of the child component is supplied by the parent component.
</ChildComponent>


//child component
<div class="panel panel-success">
  <div class="panel-heading">@Title</div>
  <div class="panel-body">@ChildContent</div>
</div>
@functions {
    [Parameter]
    private string Title { get; set; }

    [Parameter]
    private RenderFragment ChildContent { get; set; }
}
```
* Segundo a documenta��o do Blazor, a propriedade `RenderFragment` deve chamar `ChildContent` por conven��o. 

## COMPONENTES CS ONLY
* Podemos criar um componente utilizando apenas uma classe C#. Ex:
```
 public class VButton: BlazorComponent{
    public const string Title = "Hello World - Class Only";
    [Parameter]
    private bool absolute { get; set; } = false;
    [Parameter]
    private RenderFragment ChildContent { get; set; }

    protected override void BuildRenderTree(RenderTreeBuilder builder){
        builder.OpenElement(1, "h1");
        builder.AddContent(2, Title);
        builder.CloseElement();
    }
}
```     

# EVENT HANDLING
* Blazor fornece event handling atrav�s da sintaxe `on<event>`. N�o coloca-se os par�metros. Exemplo: 
```
<button class="btn btn-primary" onclick="@UpdateHeading">
    Update heading
</button>

@functions {
    void UpdateHeading(UIMouseEventArgs e){
        ...
    }
}
```

# CLASSES
* Pode-se colocar uma classe dentro de @functions, mas geralmente N�O � NECESS�RIO. As fun��es definidas dentro de @functions j� pertencem a classe com o nome o arquivo. Caso queira adicionar mais uma classe, adicione da seguinte maneira:
```
@functions{
    class Bozo{
        public int i{get;set;}
    }
}
```

# CSS em C#
* Pode-se, opcionalmente, utilizar um css definido no c�digo C#. Ex:
```
 <td style="font-style:@headingFontStyle">ola</td>
 
 @functions {
    private string headingFontStyle = "normal";
 }
```



# LISTAS
Para realizar um iterate sobre uma lista: 
```
<ul>
@foreach(var item in list){
    <li>@item.name</li>
}
</ul>
```


# BUTTON
```
<button onclick=@{MinhaClasse.AddTodo(new TodoItem{name="fazer hoje"});}>
```

# NAVLINK
* Navlink � um componente do blazor. � um link que indica a rota para onde ir ao clicar sobre ele. Ex:
```
   <NavLink class="nav-link" href="/counter">
        <span class="oi oi-plus" aria-hidden="true"></span> Counter
    </NavLink>

      <NavLink class="nav-link" href="/" Match=NavLinkMatch.All>
        <span class="oi oi-home" aria-hidden="true"></span> Home
    </NavLink>
```            

# BINDING
* Pode-se fazer um bind sobre o que est� sendo digitado no input da seguinte maneira:
```
<input bind="@textoInput" placeholder="Type here..."/>
@functions{
    string textoInput;
}
```
* Sempre que o conte�do do input for alterado, � alterado o conte�do da vari�vel textoInput. O inverso n�o � v�lido. Caso o input j� tenha sido renderizado, caso o conte�do de textoInput seja alterado, o conte�do do input n�o � alterado. Para que isso aconte�a, � necess�rio executar `StateHasChanged();` 

## CHILD BINDING
Podemos fazer child binding. Para isso, o child deve ter um action `<Atributo>Changed` para o atributo que � bindable. Veja o exemplo abaixo:
```
//parent
<ChildComponent bind-Year="@ParentYear" />
@functions {
    public int ParentYear { get; set; } = 1979;
}

//child 
<div> ... </div>
@functions {
    public int Year { get; set; }
    public Action<int> YearChanged { get; set; }
}
``` 

# FORMATA��O DE DATAS
* Podemos formatar datas da seguinte maneira: 
```
<input bind="@StartDate" format-value="yyyy-MM-dd" />

@functions {
    public DateTime StartDate { get; set; } = new DateTime(2020, 1, 1);
}
```

# LIFECYCLE METHODS
* Seguem os m�todos do blazor e a sequencia de execu��o: 
1) OnInitAsync ou OnInit � executado assim que o componente � initializado. 
1) OnParametersSetAsync ou OnParametersSet � executado assim que o componente recebe par�metros do parent e atribui a suas propriedades. � executado ap�s o OnInit.
1) OnAfterRenderAsync ou OnAfterRender � executado cada vez que o componente finaliza a renderiza��o. Refer�ncias do componente/elemento s�o preenchidas neste momento.
* Caso crie o m�todo OnParametersSet, adicione a linha `base.SetParameters(parameters);` dentro do m�todo para que todos os valores sejam obrigatoriamente passados. 
* O m�todo `protected override bool ShouldRender()` pode ser utilizado para suprimir o refresh da UI. Se retorna true, a UI � refreshed. Independente do retorno, o componente � sempre renderizado.
* Se o componente implementa IDisposable, o m�todo Dispose() � executado quando o componente � removido da UI. Ex: 
```
@using System
@implements IDisposable

...
@functions {
    public void Dispose(){
        ...
    }
}
```  
 

# INTEROPERABILIDADE COM JAVASCRIPT
* Podemos mapear fun��es em c# para javascript da seguinte maneira: 
```
No html ou em um arquivo js em um blazorlib: 
<script>
    Blazor.registerFunction('goBack', () => {
        window.history.back();
        return true;
    });
</script>

No C#
protected void GoBack(){
    RegisteredFunction.Invoke<bool>("goBack");
}
```    
* Assim, podemos executar a fun��o `window.history.back()` executando a fun��o C# `GoBack();`
* Outro exemplo. Para setar o focus de um input: 
```
Declarar no js de um blazorlib: 
Blazor.registerFunction('BlazorFocus.FocusElement', function (element) {
    element.focus();
});

Declarar no cs do blazorlib:
 public static class ExampleJsInterop{
    public static void Focus(this ElementRef elementRef){
        RegisteredFunction.Invoke<object>("BlazorFocus.FocusElement", elementRef);
    }
}

Criar o input no .cshtml do App:
<input ref="meuInput" />

@functions {
    ElementRef meuInput;

    void SetFocus(){
        meuInput.Focus();
    }    
}    
```
* Podemos acessar componentes Blazor definidos no app assim:
```
<MyLoginDialog ref="loginDialog"/>

@functions {
    MyLoginDialog loginDialog;
    void OnSomething(){
        loginDialog.Show();
    }
}
```
* Caso requeira re-renderizar o loginDialog acima porque passou um novo valor para um label, por exemplo, execute `StateHasChanged();` no cs da fun��o que recebe o valor no MyLoginDialog.
* O ref � nulo enquanto o componente n�o � renderizado. 
* Ap�s o componente ser renderizado, podemos capturar atrav�s da fun��o `protected override void OnAfterRender()` ou `protected override void OnAfterRenderAsync()`

# INHERITANCE E CODE-BEHIND
* Podemos dividir um componente Blazor .cshtml em dois arquivos. Um com o conte�do .cshtml e outro com as fun��es em C#. Exemplo: 
```
//blazorrocks.cshtml 

@page "/BlazorRocks"
@inherits BlazorRocksBase
<h1>@BlazorRocksText</h1>

//BlazorRocksBase.cs 
using Microsoft.AspNetCore.Blazor.Components;
public class BlazorRocksBase : BlazorComponent{
    public string BlazorRocksText { get; set; } = "Blazor rocks the browser!";
}
```
* Note que a classe base deve derivar de BlazorComponent. 

# SERVICES (DEPENDENCY INJECTION)
* Servi�os s�o disponibilizados aos componentes via dependency injection. 
* O dependency injection permite que um �nico singleton seja compartilhado entre os componentes. � configurado durante o startup do app em Program.cs.
* Os services podem ser: 
1) Singleton: Todos os componentes recebem a mesma instancia do servi�o. 
1) Transient: Cada componente que requisita o servi�o recebe uma nova inst�ncia. 
* Blazor oferece alguns default services que s�o adicionados automaticamente ao service collection. Entre eles, est�o: 
1) IUriHelper: Helper que auxilia no roteamento. 
1) HttpClient: Fornece um cliente HTTP. Note que o  HttpClient.BaseAddress � automaticamente setado ao prefixo base do app.
* Um detalhe interessante � que podemos fazer um inject de uma interface em um componente. Ex: `@inject IDataAccess DataRepository`. A propriedade DataRepository recebe uma implementa��o 
* Outro detalhe � que um componente que herda outro componente n�o precisa usar o @inject. Apenas o componente base precisa do @inject. 
* Services podem utilizar outros services, mas n�o podem usar @inject. Devem inserir o service como par�metro no constructor. Ex: `public Repository(HttpClient client)`.

* Por exemplo, para usar o httpclient, basta inserir no .cshtml: `@inject HttpClient Http;`
* Podemos criar um novo servi�o da seguinte maneira:
```
Adicionar no Program.cs:  
var serviceProvider = new BrowserServiceProvider(configure =>
{
    // Add any custom services here
    configure.AddSingleton<HeroesServiceClient>();
});

Adicionar o arquivo HeroesServiceClient.cs na pasta Services:

 public class HeroesServiceClient{
    private readonly HttpClient _httpClient;
    public HeroesServiceClient(HttpClient httpClient){
        _httpClient = httpClient;
    }

    public async Task<List<Hero>> GetHeroes(){
        return await _httpClient.GetJsonAsync<List<Hero>>("/api/heroes");
    }

    public async Task<Hero> GetHero(string id){
        return await _httpClient.GetJsonAsync<Hero>($"/api/heroes/{id}");
    }

    public async Task DeleteHero(string heroId){
        await _httpClient.DeleteAsync($"/api/heroes/{heroId}");
    }
}
```

# ROUTER
*Pode-se navegar para outra p�gina a partir do cs atrav�s do c�digo abaixo:
```
@inject Microsoft.AspNetCore.Blazor.Services.IUriHelper UriHelper

void NavigateToPage1(){
    UriHelper.NavigateTo("/page1");
}
```
Outra op��o � navegar para outra p�gina atrav�s do NavLink. Ex:
```
Inserir no arquivo .html:

<NavLink class="nav-link" href="/counter">
    <span class="oi oi-plus" aria-hidden="true"></span> Counter
</NavLink>
```      
* Um componente pode ser acessado atrav�s de v�rias rotas. Ex:
```
@page "/BlazorRoute" <--veja!
@page "/DifferentBlazorRoute" <--veja!
<h1>Blazor routing</h1>
```      
* Um componente pode receber par�metros de rota assim: 
```
@page "/RouteParameter"
@page "/RouteParameter/{text}" <--veja!

<h1>Blazor is @Text!</h1>

@functions {
    [Parameter]
    private string Text { get; set; } = "fantastic";
}
```
* Par�metros de rota opcionais n�o � suportado. Para isso, cria-se v�rios @page como o exemplo acima. 

# RAZOR
* Razor directives dispon�veis s�o:
<table>
<tr><td>@functions</td>
<td>Adiciona um bloco de fun��e ao componente</td></tr>

<tr><td>@implements</td>
<td>Implementa a interface no componente</td></tr>

<tr><td>@inherits</td>
<td>Herda a classe...</td></tr>

<tr><td>@inject</td>
<td>Herda a classe...</td></tr>

<tr><td>@inject</td>
<td>Injeta o servi�o no componente. </td></tr>

<tr><td>@layout</td>
<td>Especifica o componente de layout. S�o usados para evitar duplicidade de c�digo e inconsist�ncia</td></tr>

<tr><td>@page</td>
<td>Indica que o componente manipula requests diretamente, ou sej� � rote�vel. </td></tr>

<tr><td>@using</td>
<td>Utiliza o assembly/namespace... </td></tr>

<tr><td>@addTagHelper</td>
<td>Utiliza o componente em um assembly diferente do assembly do app.</td></tr>

</table>

# ATRIBUTOS CONDICIONAIS
* Podemos especificar condicionamente um atributo assim: 
```
<input type="checkbox" checked="@IsCompleted" />
@functions {
    public bool IsCompleted { get; set; }
}
```
No c�digo acima, caso IsCompleted=true, o componente � renderizado assim: <br>
`<input type="checkbox" checked />`
<br>Caso contr�rio: <br>
`<input type="checkbox" />`

# LAYOUTS
* Mais info [aqui](https://blazor.net/docs/layouts.html)
* Tecnicamente um layout � apenas um componente blazor. Um Layout pode conter data binding, dependency injection e outras funcionalidades. � usado para evitar duplica��o de c�digo em todas as p�ginas que utilizam o mesmo layout. H� apenas duas diferen�as de um componente convencional: 
1) Um componente de layout deve herdar  BlazorLayoutComponent.  BlazorLayoutComponent define uma propriedade Body que contem o conte�do a ser renderizado dentro do layout. 
1) Um componente de layout indica onde o conte�do de Body � renderizado atrav�s de @Body. Ex:
```
@inherits BlazorLayoutComponent
<header>
    <h1>ERP Master 3000</h1>
</header>

<nav>
    <a href="/master-data">Master Data Management</a>
    <a href="/invoicing">Invoicing</a>
    <a href="/accounting">Accounting</a>
</nav>

@Body

<footer>
    &copy; by @CopyrightMessage
</footer>
@functions {
    public string CopyrightMessage { get; set; }
    ...
}
```
* Para utilizar o layout criado acima em um componente, crie o componente conforme abaixo. O componente abaixo � renderizado dentro de @Body no layout: 
```
@layout MasterLayout
@page "/master-data"
<h2>Master Data Management</h2>
...
```
## LAYOUT POR PASTA 
* Cada pasta de um app Blazor pode, opcionalmente, conter um arquivo de template _ViewImports.cshtml. 
* O compilador blazor inclui todos os directives definidos nesse arquivo em todos os arquivos .cshtml da mesma pasta e subpastas. Assim, caso o _ViewImports.cshtml contenha a linha `@layout MainLayout`, todos os componentes na mesma pasta e subpastas v�o utilizar o layout MainLayout. 

## NESTED LAYOUTS
* Blazor suporta nested layouts conforme o exemplo abaixo: 
```
//CustomersComponent.cshtml:
@layout MasterDataLayout
@page "/master-data/customers"
<h1>Customer Maintenance</h1>
...
```

```
//MasterDataLayout.cshtml:
@layout MainLayout
@inherits BlazorLayoutComponent

<nav>
    <!-- Menu structure of master data module -->
    ...
</nav>

@Body
```

```
//MainLayout.cshtml:    
@inherits BlazorLayoutComponent

<header>...</header>
<nav>...</nav>

@Body
```

# PUBLISH
`dotnet publish -c Release`

**author** [Bruno Vacare Tezine](mailto:bruno.vacare.tezine@accenture.com)