﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Entities;
using BlazorApp.Components;
#endregion

namespace BlazorApp.Codes {

    public interface IBooksClient {
        Task<CPagedResult<EBook>> ListBooks(int page);
        Task<EBook> GetBook(int id);
        Task SaveBook(EBook book);
        Task DeleteBook(EBook book);
        Task DeleteBook(int id);
    }
}
