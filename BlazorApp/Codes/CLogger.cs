﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp {

    static public class CLogger {
        #region LogDebug
        static public void LogDebug(string msg) {
            Console.WriteLine(msg);
        }
        #endregion

        #region LogError
        static public void LogError(string msg) {
            //Debug.WriteLine ainda não era suportado
            Console.WriteLine(msg);
        }
        #endregion
    }
}
