﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Components {
    public class CPagedResult<T> : CPagedResultBase where T : class {
        public IList<T> Results { get; set; }

        public CPagedResult() {
            Results = new List<T>();
        }
    }
}
