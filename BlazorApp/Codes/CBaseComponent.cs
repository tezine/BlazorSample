﻿#region Imports
using Microsoft.AspNetCore.Blazor.Components;
using Microsoft.AspNetCore.Blazor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Codes {
    public abstract class CBaseComponent : BlazorComponent {
        [Inject]
        protected IUriHelper UriHelper { get; set; }

        [Inject]
        protected IBooksClient BooksClient { get; set; }
    }
}
