﻿#region Imports
using BlazorApp.Components;
using BlazorApp.Entities;
using Microsoft.AspNetCore.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
#endregion

namespace BlazorApp.Codes {
    public class CBooksClient : IBooksClient {

        #region Fields
        private List<EBook> booksList = new List<EBook>();
        #endregion

        #region Constructor
        public CBooksClient() {
            for(int i = 0; i < 1000; i++) {
                booksList.Add(new EBook { Id = i, Title = "Livro" + i, ISBN = "23432423" + i });
            }
        }
        #endregion

        #region DeleteBook
        public async Task DeleteBook(EBook book) {
            booksList.RemoveAll(x => x.Id == book.Id);
        }
        #endregion

        #region DeleteBook
        public async Task DeleteBook(int id) {
            booksList.RemoveAll(x => x.Id == id);
        }
        #endregion

        #region ListBooks
        public async Task<CPagedResult<EBook>> ListBooks(int page) {
            var x = new CPagedResult<EBook>();
            x.Results = booksList.Skip(page).Take(50).ToList();
            x.CurrentPage = page;
            x.PageCount = booksList.Count / 50;
            x.PageSize = 50;
            x.RowCount = booksList.Count;
            return x;
            //return await _httpClient.GetJsonAsync<CPagedResult<EBook>>("/Books/Index/page/" + page);
        } 
        #endregion

        #region GetBook
        public async Task<EBook> GetBook(int id) {
            return booksList.Where<EBook>(x => x.Id == id).FirstOrDefault();
        } 
        #endregion

        #region SaveBook
        public async Task SaveBook(EBook book) {
            for (var i = 0; i < booksList.Count; i++) {
                if (booksList[i].Id == book.Id) {
                    booksList[i] = book;
                    return;
                }
            }
            booksList.Add(book);
        } 
        #endregion
    }
}
