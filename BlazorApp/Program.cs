﻿#region Imports
using Microsoft.AspNetCore.Blazor.Browser.Rendering;
using Microsoft.AspNetCore.Blazor.Browser.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using BlazorRedux;
using BlazorApp.Codes;
#endregion

namespace BlazorApp{
    public class Program{

        #region Main
        static void Main(string[] args) {
            var serviceProvider = new BrowserServiceProvider(services => {
                //Add any custom services here

                //Redux abaixo
                services.AddReduxStore<CReduxFetchDataState, IAction>(new CReduxFetchDataState(), CReduxReducers.RootReducer, options => {
                    options.GetLocation = state => state.Location;
                });

                //CBooksClient singleton abaixo
                services.AddSingleton<IBooksClient, CBooksClient>();
            });
            new BrowserRenderer(serviceProvider).AddComponent<App>("app");
        } 
        #endregion
    }
}
