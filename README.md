# BlazorSample
* Solução Blazor contendo um App Blazor e uma Lib Blazor.
* Demonstram as principais funcionalidades do Blazor e servem como referência para criação de projetos reais.
* Há também uma apresentação [aqui](ApresentaçãoBlazor.pptx) e um README mais técnico [aqui](BlazorApp/README.md)

## FUNCIONALIDADES
* O projeto BlazorApp apresenta os itens à seguir: <br><br>
1) Dependency Injection.<br>
1) Integração com Libs.<br>
1) Utilização de pacotes via Nuget. <br>
1) Redux.<br>
1) Layouts. <br>
1) Componentes. <br>
1) Integração com Javascript. <br>
1) Router. <br>
1) Data binding. <br>
1) Exemplo de requests REST. <br>
1) Lifecycle methods. <br>
1) DataTable com paginação. <br>
1) Integração com CSS.<br>
...

## REFERÊNCIAS
* https://blazor.net/
* https://learn-blazor.com/
* https://codedaze.io/
* https://github.com/aspnet/Blazor
* https://github.com/torhovland/blazor-redux
* http://www.kashyapas.com/2018/04/10/blazor-tour-of-heroes-spa-using-net-blazor/
* https://github.com/aspnet/samples/tree/master/samples/aspnetcore/blazor
* https://docs.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-2.1
* http://gunnarpeipman.com/
