﻿using Microsoft.AspNetCore.Blazor.Browser.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorLib
{
    public class MyJSClass
    {
        #region GetWindowWidth
        public static int GetWindowWidth() {
            return RegisteredFunction.Invoke<int>("BlazorLib.MyJSClass.getWindowWidth");
        }
        #endregion
    }
}
