﻿#region Imports
/**
 * @brief Esta classe estabelece a comunicação com o javascript. 
 * @author Bruno Tezine -> bruno.vacare.tezine@accenture.com
 */
using System;
using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Blazor.Browser.Interop; 
#endregion

namespace BlazorLib{

    public static class JSInterop{

        #region Prompt
        public static string Prompt(string message) {
            return RegisteredFunction.Invoke<string>("BlazorLib.JSInterop.Prompt", message);
        }
        #endregion

        #region Alert
        public static string Alert(string message) {
            return RegisteredFunction.Invoke<string>("BlazorLib.JSInterop.Alert", message);
        }
        #endregion

        #region Focus
        public static string Focus(this ElementRef elementRef) {
            return RegisteredFunction.Invoke<string>("BlazorLib.JSInterop.Focus", elementRef);
        }
        #endregion

        #region SetDocumentBackgroundColor
        public static bool SetDocumentBackgroundColor(string color) {
            return RegisteredFunction.Invoke<bool>("BlazorLib.JSInterop.setDocumentBackgroundColor", "orange");
        } 
        #endregion

    }
}
