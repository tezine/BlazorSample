﻿// This file is to show how a library package may provide JavaScript interop features
// wrapped in a .NET API
//todo nao funciona isso ainda import { MyClass } from "MyJSClass.js";

//#region Prompt
Blazor.registerFunction('BlazorLib.JSInterop.Prompt', function (message) {
    return prompt(message, 'Type anything here');
});
//#endregion 

//#region Prompt
Blazor.registerFunction('BlazorLib.JSInterop.Alert', function (message) {
    return alert(message);
});
//#endregion 

//#region Focus
Blazor.registerFunction('BlazorLib.JSInterop.Focus', function (element) {
    element.focus();
});
//#endregion 

//#region setDocumentBackgroundColor
Blazor.registerFunction('BlazorLib.JSInterop.setDocumentBackgroundColor', (color) => {
    document.body.style.backgroundColor = color;
    // Your function currently has to return something. For demo purposes, we just return `true`.
    return true;
});
//#endregion 

//===========================ACESSOS A UMA CLASSE JAVASCRIPT ABAIXO=============================

//#region MyJSClass.getWindowWidth
Blazor.registerFunction('BlazorLib.MyJSClass.getWindowWidth', () => {
    console.log('vai verificar largura');
    return MyClass.getWindowWidth();
});
//#endregion 